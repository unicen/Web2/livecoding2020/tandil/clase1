<?php

require_once "operaciones.php";

if(isset($_GET["valor1"]) && isset($_GET["valor2"])){
    $valor1 = $_GET["valor1"];
    $valor2 = $_GET["valor2"];

    $operacion = $_GET["operacion"];

    switch ($operacion){
        case "restar":
           $resultado = restar($valor1, $valor2);
        break;
        case "sumar":
            $resultado = sumar($valor1, $valor2);
        break;
        case "multiplicar":
            $resultado = multiplicar($valor1, $valor2);
        break;
        case "dividir":
            $resultado = dividir($valor1, $valor2);
        break;
    }

    echo "Resultado de " . $operacion . " " . $valor1 . " y " . $valor2 . " es " .$resultado;

}


?>