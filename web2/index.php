<?php

    $titulo = "Hola mundo!";
    echo  $titulo;

    $aBool = true;   // boolean
  $name  = "Juan";  // string
  $lastName = "Perez";  // string 
  $cont = 12;     // integer
  echo "</br>";
  echo "$name, $lastName";  // outputs "Juan, Perez"
  echo "</br> CONSTANTES </br>";
  define("SALUDO", "Hello world.");


    echo SALUDO; // outputs "Hello world."

    echo "</br> ARREGLOS </br>";
    $cars = array("Volvo", "BMW", "Toyota");
    // Asignación manual
    $cars[0] = "Volvo";
    $cars[1] = "BMW";
    $cars[2] = "Toyota";
    $count = count($cars);  // 3 (int)
    echo $count;

    echo "</br> ARREGLOS ASO</br>";
    $edades = array( 
        "juan" => 35, 
        "nico" => 17, 
        "julia" => 23,
        "javito" => 35
    ); 
    echo $edades["juan"]; //imprime 35
    echo $edades["julia"]; //imprime 23
    

    echo "</br> ARREGLOS EJERCICIO</br>";
    define("NOMBRE", "juan");
    echo $edades[NOMBRE];

    echo "</br> ARREGLOS PUSH PULL</br>";
    array_push($cars, "Chevrolet");
    $count = count($cars);  
    echo $count;
    echo $cars[3];
    array_pop($cars);
    echo count($cars);
    echo $cars[3];

    echo "</br> FUNCIONES </br>";

    /**
    * Calcular el promedio de los valores de un arreglo
    * adsasd
    **/
    function promedioEdad($ed){ 
        $promedio = array_sum($ed) / count($ed);
        return $promedio;
    } 

    $prom = promedioEdad($edades);
    echo $prom;

    $a = 1; /* ámbito global */
    function test() {
       GLOBAL $a;
       echo $a; 
    }
    test();

    echo "</br> FOR </br>";

    $autos = array("Volvo", "BMW", "Toyota", "Chevrolet", "Ford");
    $count = count($autos);  // 5 (int)

    for ($i = 0; $i < $count; $i++) {
        echo  "<li>" . $autos[$i] . "</li>";
      }
      
    $edades2 = array( 
        "juan" => 35, 
        "nico" => 17, 
        "julia" => 23,
        "javito" => 35
    ); 
    foreach ($edades2 as $edad) { 
        echo "<li>".$edad."</li>";      
    } 
      
    echo "</br> TEXTO </br>";
    
    echo strlen("Hello world!"); // outputs 12 
  echo str_word_count("Hello world!"); // outputs 2 
  echo strrev("Hello world!"); // outputs !dlrow olleH 
  echo strpos("Hello world!", "world"); // outputs 6 
  echo str_replace("world", "Dolly", "Hello world!");

?>
    
